#source: https://www.youtube.com/watch?v=DtGrdB8wQ_8
# CODEDIRS=.	# Look here for c files (. := working directory)
INCDIRS=-I.	# Look here for header files

CC=gcc
OPT=
CFLAGS=-Wall -Wextra $(INCDIRS) $(OPT)

PAM_CFLAGS=-fPIC -DPIC -shared -rdynamic $(INCDIRS) $(OPT) $(CFLAGS) -Wno-unused-parameter
SHA_CFLAGS=$(CFLAGS)

PAM_BIN=pam_allow_deny.so
PAM_SRC=sha256-hash-3.0.c pam_allow_deny.c
PAM_OBJ = $(PAM_SRC:.c=.o)


# For utility executable to get sha256 hashes for names
SHA_BIN=sha256sum
SHA_SRC=sha256-hash-3.0.c sha256sum.c
SHA_OBJ = $(SHA_SRC:.c=.o)

# path to pam modules
security=/usr/lib/x86_64-linux-gnu/security

all: $(PAM_BIN) $(SHA_BIN)

install: all
	sudo cp pam_allow_deny.so "${security}"/ \
		&& sudo chown root:root "${security}"/pam_allow_deny.so \
		&& sudo chmod 755 "${security}"/pam_allow_deny.so
	sudo cp sha256sum /usr/local/bin \
		&& sudo chown root:root /usr/local/bin/sha256sum \
		&& sudo chmod +x /usr/local/bin/sha256sum


$(PAM_BIN): $(PAM_SRC)
	CC="$(CC)" CFLAGS="$(PAM_CFLAGS)" BIN="$(PAM_BIN)" SRC="$(PAM_SRC)" OBJ="$(PAM_OBJ)" \
	make --environment-overrides --file=Makefile-sha $(PAM_BIN)

$(SHA_BIN): $(SHA_SRC)
	CC="$(CC)" CFLAGS="$(SHA_CFLAGS)" BIN="$(SHA_BIN)" SRC="$(SHA_SRC)" OBJ="$(SHA_OBJ)" \
	make --environment-overrides --file=Makefile-sha $(SHA_BIN)

# Place holder for source files
%.c:

clean:
	rm -rf $(PAM_BIN) $(PAM_OBJ) $(SHA_BIN) $(SHA_OBJ)

.PHONY: all install clean
