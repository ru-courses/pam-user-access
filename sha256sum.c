#include "sha256-hash-3.0.h"

/*
 * Hash all command line args with sha256 and print them to stdout.
 */
int main(int argc, char **argv) {
    int res;
    int errors = 0;

    if (argc < 2) {
        fprintf(stderr,
                "Description: Each commandline argument will be hashed "
                "with sha256 and the result will be printed to stdout. Each "
                "hash will appear on a seperate line.\n"
                "To easily creating allow and deny file use:\n"
                "%s name1 name2 name3 ... >> /tmp/users_[allow "
                "| deny ]"
                "\nUsage: %s [ name1 name2 ... ]\n"
                , argv[0], argv[0]);
        return -1;
    }

    /*
     * For some reason the for loop only runs 1 time regardless of the
     * number of command line args.
     */
    /* unsigned char md_value[EVP_MAX_MD_SIZE]; */
    /* unsigned int md_len = 0; */
    /* for(int i=1; i<argc; i++) { */
    /*     res = shahash_bits(argv[i], md_value, &md_len); */
    /*     if(res == 0) { */
    /*         for (i = 0; i < (int) md_len; i++) */
    /*             printf("%02x", md_value[i]); */
    /*         printf("\n"); */
    /*     } else { */
    /*         errors += 1; */
    /*         fprintf(stderr, "Error occured while hashing %s.\n", argv[i]); */
    /*     } */
    /* } */

    char *out;
    unsigned int out_len = 0;
    for(int i=1; i<argc; i++) {
        res = shahash(argv[i], &out, &out_len);
        if(res == 0) {
            printf("%s\n", out);
        } else {
            errors += 1;
            fprintf(stderr, "Error occured while hashing %s.\n", argv[i]);
        }
        free(out);
    }

    return errors;
}
