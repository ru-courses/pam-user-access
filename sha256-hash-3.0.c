/* Source: https://www.openssl.org/docs/man3.0/man7/crypto.html
 * To compile use:
 * gcc -Wall -Wextra -lcrypto <name>.c
 *
 * this code is written for openssl 3.0
 */

#include "sha256-hash-3.0.h"

// FIX: return value of snprintf to check it copied correctly
void hex_to_str(const unsigned char *md_value, const int md_len, char *out, const int out_len) {
    for(int i=0;i<md_len;i++) {
        snprintf(&out[i*2], out_len-(i*2), "%02X", md_value[i]);
    }
}

/*
 * Hash PLAIN and store the digest is MD_VALUE and the digest length
 * in MD_LEN.
 */
int shahash_bits(const char *plain, unsigned char *md_value, unsigned int *md_len) {
    EVP_MD_CTX *ctx = NULL;
    EVP_MD *sha256 = NULL;

    /* convert plain to array of hex */
    unsigned char msg[strlen(plain)+1];

    /* unsigned int md_len = 0; */
    unsigned char *outdigest = NULL;
    int ret = 1;

    /* assume plain only contains ascii value of 33 till 126 and can therefore
     * simply be type casted to unsigned char. */
    strncpy((char *)msg, plain, strlen(plain)+1);
    /* if (msg == NULL) { */
    /*     goto err; */
    /* } */

    /* Create a context for the digest operation */
    ctx = EVP_MD_CTX_new();
    if (ctx == NULL)
        goto err;

    sha256 = EVP_MD_fetch(NULL, "SHA256", NULL);
    if (sha256 == NULL)
        goto err;

   /* Initialise the digest operation */
   if (!EVP_DigestInit_ex(ctx, sha256, NULL))
       goto err;

    /*
     * Pass the message to be digested. This can be passed in over
     * multiple EVP_DigestUpdate calls if necessary
     */
    if (!EVP_DigestUpdate(ctx, msg, sizeof(msg)))
        goto err;

    /* Allocate the output buffer */
    outdigest = OPENSSL_malloc(EVP_MD_get_size(sha256));
    if (outdigest == NULL)
        goto err;

    /* Now calculate the digest itself */
    if (!EVP_DigestFinal_ex(ctx, outdigest, md_len))
        goto err;

    /* Print out the digest result */
    /* BIO_dump_fp(stdout, outdigest, *md_len); */

    ret = 0;

    if (*md_len <= EVP_MAX_MD_SIZE) {
        memcpy(md_value, outdigest, *md_len);
    } else {
        ret = 1;
    }

 err:
    /* Clean up all the resources we allocated */
    OPENSSL_free(outdigest);
    EVP_MD_free(sha256);
    EVP_MD_CTX_free(ctx);
    if (ret != 0) {
        ERR_print_errors_fp(stderr);
    }
    return ret;
}

int shahash(const char *msg, char **out, unsigned int *out_len) {
    /* 
     * char **out must be freed by the caller.
     */

    unsigned char md_value[EVP_MAX_MD_SIZE];
    unsigned int md_len, h;

    h = shahash_bits(msg, md_value, &md_len);
    if (h != 0) {
        return 2;
    }

    *out_len = md_len*2 + 1;
    *out = (char *) malloc(*out_len * sizeof(char));

    if (*out == NULL) {
        fprintf(stderr, "Error allocating memory for hash string.\n");
        return 1;
    }

    hex_to_str(md_value, md_len, *out, *out_len);
    return 0;
}
