/* Define which PAM interfaces we provide */
#define PAM_SM_ACCOUNT
#define PAM_SM_AUTH
#define PAM_SM_PASSWORD
#define PAM_SM_SESSION

#define _GNU_SOURCE

#include <errno.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Include PAM headers */
#include <security/pam_appl.h>
#include <security/pam_modules.h>

#include "sha256-hash-3.0.h"


// FIX: file_info.st_uid and gid do not return the expected values
int
root_owned(const char *file, int *out) {
    /**
     * returns 0 if file has owner uid = gid = 0 (aka the owner is root)  and 
     * 1 otherwise. 
     */
    struct stat file_info;
    const unsigned int
        root_uid = 0,
        root_gid = 0;

    stat(file, &file_info);
    if (errno != 0) {
        return errno;
    }

    return (file_info.st_uid != root_uid || file_info.st_gid != root_gid);
}

int
find_user(const char *user, FILE *fp) {
    /**
     * returns 0 if USER is a line in FP or 1 otherwise.
     */
    char *line = NULL;
    size_t len = 0;
    ssize_t nread;

    while ((nread = getline(&line, &len, fp)) != -1) {
        strtok(line, "\n"); /* remove trailing \n if present */

        if (strcmp(line, user) == 0) {
            free(line);
            return 0;
        }
    }
    free(line);
    return 1;
}

int
allow_deny(char *user_hash, const char *fname) {
    /**
     * Return 0 if USER_HASH is in FNAME, return 1 if not and return 2
     * on error.
     */
    FILE *fp;
    int found_user = 0;

    fp = fopen(fname, "r");
    if (fp == NULL) {
        return 2;
    }
    found_user = find_user(user_hash, fp);
    fclose(fp);
    return found_user;
}

/* PAM entry point for authentication verification */
int
pam_sm_authenticate(pam_handle_t *pamh, int flags, int argc, const char **argv) {
    /**
     * Authenticate the user. If the user is in the deny list then
     * return authentication error, if the user is in the allow
     * list then return success.
     */
    const char *ifname_allow = "/tmp/users_allow",
        *ifname_deny = "/tmp/users_deny";
    char *user = NULL,
        *user_hash;
    int pam_res,
        tmp_res;
    size_t user_len = 0;
    unsigned int user_hash_len;

    /* tmp_res = pam_get_user(pamh, &user, NULL); */
    /* if (tmp_res != PAM_SUCCESS || user == NULL) { */
    /*     return PAM_SERVICE_ERR; */
    /* } */

    // Ask for username
    printf("Enter a username: ");
    getline(&user, &user_len, stdin);
    strtok(user, "\n"); /* remove trailing \n if present */
    if (user == NULL) {
        return PAM_AUTH_ERR;
    }

    tmp_res = shahash(user, &user_hash, &user_hash_len);
    if (tmp_res != 0) {
        return PAM_SERVICE_ERR;
    }

    /* Denied settings take precendence over allowed settings */
    /* users to deny */
    tmp_res = allow_deny(user_hash, ifname_deny);
    if (tmp_res == 0) {
        pam_res = PAM_AUTH_ERR;
    } else if (tmp_res == 2) {
        pam_res = PAM_SERVICE_ERR;
    } else {
        /* users to allow */
        tmp_res = allow_deny(user_hash, ifname_allow);
        pam_res = tmp_res == 0 ? PAM_SUCCESS :
            tmp_res == 1 ? PAM_AUTH_ERR : PAM_SERVICE_ERR ;
    }

    free(user_hash);
    user_hash = NULL;

    return pam_res;
}

/* PAM entry point for accounting */
int
pam_sm_acct_mgmt(pam_handle_t *pamh, int flags, int argc, const char **argv) {
    return(PAM_IGNORE);
}

/* PAM entry point for session creation */
int
pam_sm_open_session(pam_handle_t *pamh, int flags, int argc, const char **argv) {
    return(PAM_IGNORE);
}

/* PAM entry point for session cleanup */
int
pam_sm_close_session(pam_handle_t *pamh, int flags, int argc, const char **argv) {
    return(PAM_IGNORE);
}

/*
   PAM entry point for setting user credentials (that is, to actually
   establish the authenticated user's credentials to the service provider)
 */
int
pam_sm_setcred(pam_handle_t *pamh, int flags, int argc, const char **argv) {
    return(PAM_IGNORE);
}

/* PAM entry point for authentication token (password) changes */
int
pam_sm_chauthtok(pam_handle_t *pamh, int flags, int argc, const char **argv) {
    return(PAM_IGNORE);
}
