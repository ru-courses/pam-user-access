#ifndef SHA256_HASH_H
#define SHA256_HASH_H

#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/err.h>

void hex_to_str(const unsigned char *, const int, char *, const int);
int shahash(const char *, char **, unsigned int *);
int shahash_bits(const char *, unsigned char *, unsigned int *);
#endif
